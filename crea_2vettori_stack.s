_PRINTF = 127
_GETCHAR = 117
_EXIT = 1
asciinl = 10
EOF = -1

.SECT .TEXT	

start:
	PUSH BP
	MOV  BP,SP
creavettore2:
	PUSH 1
	PUSH 0				!prova con zero successivi
	PUSH 2
	PUSH 4
	PUSH 0 			
	PUSH 0
	PUSH 0
	PUSH 9
	PUSH 11
	PUSH 3
creavettore1:
	PUSH 2
	PUSH 6
	PUSH 4
	PUSH 6
	PUSH 8
	PUSH 5
	PUSH 7
	PUSH 4
	PUSH 3
	PUSH 1
mettinumelem:
	PUSH 10
mettiaddrvet2:
	MOV AX,BP
	SUB AX,20
	PUSH AX
mettiaddrvet1:
        SUB AX,20
        PUSH AX 
       
    call esegui
    ! elimino i parametri passati alla syscall
    ADD  SP,6
    ! elimino vettori dallo stack
    ADD  SP,40
    ! ripristino il BP caricandolo dal dynamic link
    POP  BP
    ! stampo il risultato messo in AX
    PUSH AX
    PUSH pfmt
    PUSH _PRINTF
    SYS
    ! termino processo
    PUSH 0
    PUSH _EXIT
    SYS

esegui:
    PUSH BP 
    MOV BP , SP
    PUSH 0	
    PUSH 0
    call usa1
    call usa2
    MOV AX , 0
    ADD AX , -2(BP)
    ADD AX , -4(BP)
    ADD SP , 4
    POP BP
    RET
        
usa1:
    PUSH BP
    
    
    MOV CX , 4  			!numero cicli di loop1
    
    MOV BP , 4(BP)
    MOV DI , 2
    MOV DX , 0
    MOV BX , 0
    MOV AX , 0
loop1:   
    MOV AX , (BP)(DI)
    ADD BX , AX
    SHL DI , 1
    LOOP loop1

fusa1:
    POP BP 
    MOV -2(BP) , BX		!metto la variabile nello stack
    RET



usa2:
    PUSH BP
    MOV BP, 6(BP)
    MOV DI , 0
    MOV BX , 0
    MOV AX , 0
    MOV CX , 10  		!nuero di cili di cerca2


cerca2:   
    MOV AX , (BP)(DI)
    CMP CX , 1  		!compara cx con uno per vedere se ci troviamo a fine ciclo
    JE fusa2
    CMP AX , 0			!compara AX a zero per vedere se c'e' zero altrimenti 
    JZ somma			!se esiste lo zero fai un jump a somma altrimeni somma 2 a DI e quindi vede il numero successivo
    ADD DI , 2
    LOOP cerca2	


somma:
   ADD DI , 2
   MOV AX , (BP)(DI)
   SUB CX , 1
   CMP AX , 0			! compara ax che e' il successivo numero dopo lo zero con lo zero e se non lo e' prosegue nella ricerca	
   JZ somma2
   JNZ cerca2

somma2:
   ADD BX , 1
   JMP cerca2

fusa2:
    POP BP
    MOV -4(BP) , BX		!metto la variabile nello stack il risultato 
    RET 


     
.SECT .DATA
pfmt: .ASCIZ "The result is %d \n"
.SECT .BSS
STR:    .SPACE 80
